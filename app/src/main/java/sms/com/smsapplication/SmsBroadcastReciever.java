package sms.com.smsapplication;

/**
 * Created by ashishkumarpatel on 02/03/16.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;


/**
 * Below is the class which handles SMS
 */
public class SmsBroadcastReciever extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";

    //Audio Manager class which helps to set the Profile in different mode like
    //Ringing , Vibrate , Silent
    AudioManager myAudioManager;

    Context context;

    //This function will get called automatically once you received an sms
    public void onReceive(Context context, Intent intent) {


        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);


            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                //converting the sms in SmsMessage format
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                //getting sms body from the smsMessage API
                String smsBody = smsMessage.getMessageBody().toString();
                //getting the address for the sender which sent the sms
                String address = smsMessage.getOriginatingAddress();

                //below text is only for printing purpose for Toast
                smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody + "\n";

                //if we received sms boday as "Silent"
                if(smsBody.equalsIgnoreCase("Silent"))
                {
                    //now this is the time to make the mobile in silent mode
                    updateToSilentMode(context);
                }
                else  if(smsBody.equalsIgnoreCase("Vibrate"))//if we received sms boday as "Vibrate"
                {
                    //now this is the time to make the mobile in Vibrate mode
                    updateToVibrateMode(context);
                }
                else  if(smsBody.equalsIgnoreCase("Ringing")) //if we received sms boday as "Ringing"
                {
                    //now this is the time to make the mobile in Ringing Mode
                    updateToRingingMode(context);
                }


            }
            //For displaying a small popup message for few duration
            Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();
            this.context = context;

        }


        myAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

    }


    public void updateToSilentMode(Context context)
    {

        // Retrieving the Audio Manager so we can use it for updating the device Profile
        myAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        //now set the mobile profile to silent mode
        myAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);

        //For displaying a small popup message for few duration

        Toast.makeText(context,"Now in silent Mode",Toast.LENGTH_LONG).show();

    }

    public void updateToVibrateMode(Context context)
    {
        // Retrieving the Audio Manager so we can use it for updating the device Profile
        myAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        //now set the mobile profile to vibrate mode
        myAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

        //For displaying a small popup message for few duration

        Toast.makeText(context, "Now in Vibrate Mode", Toast.LENGTH_LONG).show();
    }

    public void updateToRingingMode(Context context)
    {
        // Retrieving the Audio Manager so we can use it for updating the device Profile
        myAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        //now set the mobile profile to normal/ringing mode
        myAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        //For displaying a small popup message for few duration
        
        Toast.makeText(context,"Now in Ringing Mode",Toast.LENGTH_LONG).show();
    }
}
